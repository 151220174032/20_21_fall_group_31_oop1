#pragma once
/*!
 *  @file   Input.h
 *  @brief  Reading input file and creating an output
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-12-06
 */

#ifndef INPUT_H_
#define INPUT_H_

#include <iostream>
#include <vector> 
#include <iterator>


using namespace std;

class Input {
private:
	string file_name;
	vector<string> lines;
public:
	Input(string file);
	bool ReadInput();
	vector<string> Output(bool isValidFile);
};
#endif