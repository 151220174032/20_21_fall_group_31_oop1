/*!
 *  @file   Output.cpp
 *  @brief  Creating an output file
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-12-06
 */

#include "Output.h"
#include <fstream>
#include <sstream>

 /*!
   * A constructor for Output class
   * Initialize the output file name
   */
Output::Output(string fileName)
{
	this->file_name = fileName;
}

/*!
 * Get output list from Simulation
 * Create an output file
 *
 * @param output output of the program
 */
void Output::add_to_output(vector<string> output)
{
	ofstream outFile(file_name);

	for (auto v : output)
		outFile << v;
}