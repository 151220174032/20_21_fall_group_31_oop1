/*!
 *  @file   Engine.h
 *  @brief  Header of the engine.
 *  @author Berkay Onk - 152120181110
 *  @date   2020-12-23
 */
#pragma once
#include"Valve.h"
#include<iostream>
#include<chrono>
#include<thread>
#include"Tank.h"
#include<vector>
#include "Observer.h"
using namespace std;

class Engine : public Observer {
    const double fuel_per_second = 5.5;
    bool status;
    Valve connectedValve;
    Tank currentConnectedTank;
public:
    Engine();
    void stop_engine();
    void start_engine();
    void give_back_fuel(double);
    bool getStatus();
    void consume();
    int wait(int);
    void connect_fuel_tank_to_engine(Tank);
    void disconnect_fuel_tank_from_engine(int);
    vector <string> list_connected_tanks();
    vector <Tank> connectedTankslist;
    Tank internalTank;
    virtual string update(bool status) override;
};