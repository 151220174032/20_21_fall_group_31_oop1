/*!
 *  @file   Main.cpp
 *  @brief  Runs the whole program
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2020-12-24
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include "Input.h"
#include "Simulation.h"

using namespace std;

int main(int argc, char* argv[]) {
    string inputFile;
    string outputFile;
    if (argc > 2) {
        inputFile = argv[1];
        outputFile = argv[2];
    }
    Input newFile(inputFile);
    Simulation newSim;
    vector<string> output;
    bool isFileValid = newFile.ReadInput();
    bool isOutputValid;

    output = newFile.Output(isFileValid);

    isOutputValid = newSim.get_commands(output);

    newSim.execute(isOutputValid,outputFile);

}
