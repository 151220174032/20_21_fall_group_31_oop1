/*!
 *  @file   FuelTanks.cpp
 *  @brief  Do the neccessary operations for FuelTanks
 *  @author Batuhan Hesapdar - 152120181112
 *  @date   2020-12-24
 */

#include "Tank.h"
#include "FuelTanks.h"
#include <vector>
#include <iostream>
#include <string>
using namespace std;

/*!
 * A constructor for FuelTanks class
 * Initializing tanks vector
 */
FuelTanks::FuelTanks()
{
	tanks = vector<Tank>();
}

/*!
 * Adds new Tank object to tanks vector list.
 *
 * @param capacity amount of the new Tank object's capacity
 */
void FuelTanks::add_fuel_tank(double capacity)
{
	Tank newTank(capacity);
	tanks.push_back(newTank);
}

/*!
 * Listing all of the fuel tanks information
 *
 * @return a vector list that include fuel tanks informations.
 */
vector <string> FuelTanks::list_fuel_tanks()
{
	vector<string> out;
	out.push_back("\nHere are the fuel tanks informations");
	for (int i = 0; i < tanks.size(); i++)
	{
		if (tanks.at(i).isItConnected()) {
			out.push_back("\nTank " + to_string(i + 1) + " is connected to engine.");
		}
		else {
			out.push_back("\nTank " + to_string(i + 1) + " information: ");
			out.push_back(" capacity is: " + to_string(tanks.at(i).get_capacity()));
			out.push_back(" fuel_quantity is: " + to_string(tanks.at(i).getFuel_quentity()));
			if (tanks.at(i).get_broken_status())
				out.push_back(" Is broken: true");
			else
				out.push_back(" Is broken: false");
		}
		
	}
	out.push_back("\n");
	return out;
}

/*!
 * Removes the tank object that with given id from tanks vector list.
 *
 * @param tank_id is the integer number that decide which tank will be removed
 */
void FuelTanks::remove_fuel_tank(int tank_id)
{
	tanks.erase(tanks.begin() + tank_id);
}

/*!
 * Calculating fuel tank count from tanks vector list
 *
 * @return a string that hold fuel tank count
 */
string  FuelTanks::print_fuel_tank_count()
{
	string out = "\n---There are " + to_string(tanks.size()) + " fuel tanks.---\n";
	return out;

}
/*!
 * it checks of the status and informs
 *
 * @param status status of the subject
 * @return returns status of the fueltanks observer
 */
string FuelTanks::update(bool status)
{
	if (!status) {
		string a;
		for (int i = 0; i < tanks.size(); i++) {
			a += "Tank " + to_string(i + 1) + ": Simulation stopped.\n";
			a += "Valve " + to_string(i + 1) + ": Simulation stopped.\n";
		}
		return a;
	}
}