#pragma once
/*!
 *  @file   Output.h
 *  @brief  Creating an output file
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-12-06
 */

#ifndef OUTPUT_H_
#define OUTPUT_H_

#include <iostream>
#include <vector>
#include <iterator>
using namespace std;

class Output {
private:
	string file_name;
public:
	Output(string fileName);
	void add_to_output(vector<string> output);
};
#endif