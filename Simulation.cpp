/*!
 *  @file   Simulation.cpp
 *  @brief  Some operations on the input that is read from FileHandler
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-12-06
 */

#include "Simulation.h"
#include <string>
#include <sstream>
#include "Output.h"

 /*!
  * A constructor for Simulation class
  * Initialize functions and arguments lists and seconds
  */
Simulation::Simulation(string outputFileName)
{
	this->functions = vector<string>();
	this->arguments = vector<string>();
	Out = new Output(outputFileName);
}
/*!
 * it adds a new element to observers' vector
 *
 * @param observer it is an observer that will be added to the list
 */
void Simulation::registerObserver(Observer* observer)
{
	observers.push_back(observer);
}
/*!
 * it removes an element from obsrves' vector
 *
 * @param observer it is an observer that will be removed from the list
 */
void Simulation::removeObserver(Observer* observer)
{
	auto iterator = std::find(observers.begin(), observers.end(), observer);

	if (iterator != observers.end()) {
		observers.erase(iterator);
	}
}
/*!
 * notify all observers
 *
 */
void Simulation::notifyObservers()
{
	for (int i = 0; i < observers.size(); i++) {
		out_data.push_back(observers[i]->update(false));
	}
}
/*!
 * sets state of the simulation and call notify method 
 *
 * @param status status of the simulation
 */
void Simulation::setState(bool status)
{
	sim_status = status;
	notifyObservers();
}
/*!
 * Get commands that is read from FileHandler
 * Divide the output into functions and arguments
 *
 * @param file_output a list of commands that is the output of FileHandler
 * @return if the list is empty = false, if it is not = true
 */
bool Simulation::get_commands(vector<string> file_output)
{
	if (file_output != vector<string>()) {
		string funcStr, argStr, temp;

		for (int i = 0; i < file_output.size() - 1;i++) {
			string v = file_output.at(i);
			stringstream ss(v);
			ss >> funcStr;
			if (!ss.eof()) {
				functions.push_back(funcStr);
				ss >> argStr;
				if (!ss.eof()) { ss >> temp; argStr = argStr +" "+ temp; }
				argStr.erase(argStr.end() - 1);
				arguments.push_back(argStr);
			}
			else {
				funcStr.pop_back();
				functions.push_back(funcStr);
				arguments.push_back("");
			}
		}
		return true;
	}
	return false;
}

/*!
 * Execute the functions with their arguments if they have
 *
 * @param validCommands check the validation of commands
 * @param out_file output file name
 */
void Simulation::execute(bool validCommands, string out_file)
{
	bool eng_status = false;
	double consumed = 0;

	registerObserver(&currentEngine);
	registerObserver(&currentFuelTanks);
	
	if (!validCommands) { // Check the input file
		out_data.push_back("Error: Wrong Input File!!");
		sim_status = false;
	}

	while (sim_status != false && functions.empty() != true) {
		if (functions.front() == "stop_simulation") {
			this->stop_simulation();
			out_data.push_back("\nThe simulation has been stopped.\n");
		}
		if (functions.front() == "start_engine") {
			this->currentEngine.start_engine();
			if (currentEngine.getStatus() == true)
				out_data.push_back("\nThe engine has been started.\n");
			else out_data.push_back("\nThere is not enough fuel. The engine could not be started.\n");
		}
		if (functions.front() == "fill_tank") {
			stringstream inputSs(arguments.front());
			int id;
			double quantity;
			if (inputSs >> id && inputSs >> quantity) {
				if (!currentFuelTanks.tanks.at(id - 1).isItConnected() || !currentEngine.getStatus())
					currentFuelTanks.tanks.at(id - 1).fill_tank(quantity);
				else if (currentFuelTanks.tanks.at(id - 1).isItConnected() && currentEngine.getStatus()) {
					currentEngine.connectedTankslist.at(id - 1).fill_tank(quantity);
				}
				out_data.push_back("\nTank " + to_string(id) + " is filled " + to_string(quantity) + " liters.\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "list_fuel_tanks") {
			vector <string> a = currentFuelTanks.list_fuel_tanks();
			for (auto v : a) {
				out_data.push_back(v);
			}
		}
		if (functions.front() == "list_connected_tanks") {
			vector <string> a = currentEngine.list_connected_tanks();
			for (auto v : a) {
				out_data.push_back(v);
			}
		}
		if (functions.front() == "give_back_fuel") {
			stringstream inputSs(arguments.front());
			double tmp;
			if (inputSs >> tmp) {
				currentEngine.give_back_fuel(tmp);
				out_data.push_back("\n---Fuel is gave back.---\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "connect_fuel_tank_to_engine") {
			stringstream inputSs(arguments.front());
			int tmp;
			if (inputSs >> tmp) {
				if (tmp <= currentFuelTanks.tanks.size()) {
					currentEngine.connect_fuel_tank_to_engine(currentFuelTanks.tanks.at(tmp - 1));
					currentFuelTanks.tanks.at(tmp - 1).setConnect(true);
					out_data.push_back("\nTank " + to_string(tmp) + " is connected to engine.\n");
				}
				else {
					out_data.push_back("\nTank " + to_string(tmp) + " is not found. Connection is failed.\n");
				}
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "disconnect_fuel_tank_from_engine") {
			stringstream inputSs(arguments.front());
			int tmp;
			if (inputSs >> tmp) {
				currentEngine.disconnect_fuel_tank_from_engine(tmp - 1);
				out_data.push_back("\nTank " + to_string(tmp) + " is disconnected from engine.\n");
				currentFuelTanks.tanks.at(tmp - 1).setConnect(false);
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "remove_fuel_tank") {
			stringstream inputSs(arguments.front());
			int tmp;
			if (inputSs >> tmp) {
				currentFuelTanks.remove_fuel_tank(tmp - 1);
				out_data.push_back("\nTank " + to_string(tmp) + " is removed.\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "open_valve") {
			stringstream inputSs(arguments.front());
			int tmp;
			if (inputSs >> tmp) {
				currentEngine.connectedTankslist.at(tmp - 1).connectedValve.open_valve();
				out_data.push_back("\nValve " + to_string(tmp) + " is opened.\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "close_valve") {
			stringstream inputSs(arguments.front());
			int tmp;
			if (inputSs >> tmp) {
				currentFuelTanks.tanks.at(tmp - 1).connectedValve.close_valve();
				out_data.push_back("\nValve " + to_string(tmp) + " is opened.\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "print_tank_info") {
			stringstream inputSs(arguments.front());
			int tmp;
			string temp;
			if (inputSs >> tmp) {
				if (!currentFuelTanks.tanks.at(tmp - 1).isItConnected() || !currentEngine.getStatus())
					temp = currentFuelTanks.tanks.at(tmp - 1).print_tank_info(tmp);
				else if (currentFuelTanks.tanks.at(tmp - 1).isItConnected() && currentEngine.getStatus())
					temp = currentEngine.connectedTankslist.at(tmp - 1).print_tank_info(tmp);
				out_data.push_back(temp + "\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "print_total_fuel_quantity") {
			double total_fuel=0;
			for (int i = 0; i < currentFuelTanks.tanks.size(); i++) {
				if (currentFuelTanks.tanks.at(i).isItConnected())
					total_fuel += currentEngine.connectedTankslist.at(i).getFuel_quentity();
				else
					total_fuel += currentFuelTanks.tanks.at(i).getFuel_quentity();
			}
			out_data.push_back("\nTotal fuel is: "+to_string(total_fuel)+"\n");
		}
		if (functions.front() == "print_fuel_tank_count") {
			out_data.push_back(currentFuelTanks.print_fuel_tank_count());
		}
		if (functions.front() == "stop_engine") {
			this->currentEngine.stop_engine();
			out_data.push_back("\nThe engine has been stopped.\n");
		}
		if (functions.front() == "add_fuel_tank") {
			stringstream inputSs(arguments.front());
			int input;
			if (inputSs >> input) {
				this->currentFuelTanks.add_fuel_tank(input);
				out_data.push_back("\nTank Id: " + to_string(currentFuelTanks.tanks.size()) + " Capacity: " + to_string(input) + " (Added)\n");
			}
			else out_data.push_back("\n---Incorrect input for " + functions.front() + ".---\n");
		}
		if (functions.front() == "print_total_consumed_fuel_quantity") {
			out_data.push_back("\nTotal consumed fuel is: " + to_string(consumed) + "\n");
		}
		if (currentEngine.getStatus() == true && functions.front() != "wait") {
			currentEngine.consume();
			consumed = consumed + 5.5;
			out_data.push_back("\n!!Consumption: Internal tank currently has: "+to_string(currentEngine.internalTank.getFuel_quentity()));
			out_data.push_back(" -> "+to_string(5.5)+"lt is consumed\n");
			if (!currentEngine.getStatus()) out_data.push_back("The fuel is out. So, engine has stopped.\n");
		}
		else if (currentEngine.getStatus() == true && functions.front() == "wait") {
			stringstream inputSs(arguments.front());
			int input,sec;
			inputSs >> input;
			sec= currentEngine.wait(input);
			consumed = consumed + sec * 5.5;
			out_data.push_back("\nWaiting " + to_string(sec) + " seconds...\n");
			out_data.push_back("\n!!Consumption: Internal tank currently has: " + to_string(currentEngine.internalTank.getFuel_quentity()));
			out_data.push_back(" -> " + to_string(5.5*sec) + "lt is consumed\n");
			if (!currentEngine.getStatus()) out_data.push_back("The fuel is out. So, engine has stopped.\n");
		}
	
		functions.erase(functions.begin()); arguments.erase(arguments.begin());

	}
	Out->add_to_output(out_data);

}

/*!
 * Generate a signal to terminate the program
 *
 * @return the status of the termination
 */
void Simulation::stop_simulation()
{
	setState(false);
}