/*!
 *  @file   Input.cpp
 *  @brief  Reading input file and creating an output
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-12-06
 */

#include <fstream>
#include <sstream>
#include "Input.h"

using namespace std;

/*!
  * A constructor for FileHandler class
  * Initialize the file name
  */
Input::Input(string file)
{
	this->file_name = file;
}

/*!
 * Read the input file
 * Assing the commands to lines list
 *
 * @return if the file is valid = true, if it is not = false
 */
bool Input::ReadInput()
{
	ifstream command_file(this->file_name);
	string line;

	if (command_file.is_open()) {
		while (command_file) {
			getline(command_file, line);
			lines.push_back(line);
		}
		return true;
	}
	else return false;
}

/*!
 * Check the validity of file
 * Return the list of commands if the file is valid
 *
 * @param isValidFile the validity of the file
 * @return the list of commands or empty list
 */
vector<string> Input::Output(bool isValidFile)
{
	if (isValidFile) {
		return lines;
	}
	return vector<string>();
}