/*!
 *  @file   Valve.h
 *  @brief  Including valve operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-12-25
 */
#pragma once
#include <Windows.h>
#include <string>

using namespace std;


class Valve
{
public:
	Valve();
	void open_valve();
	void close_valve();
	bool getvalveStatus();
private:
	bool valveStatus;
};