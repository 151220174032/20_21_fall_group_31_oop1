/*!
 *  @file   Simulation.h
 *  @brief  Some operations on the input that is read from FileHandler
 *  @author Omer Faruk Sahin - 151220174032
 *  @date   2020-12-06
 */
#pragma once
#include <iostream>
#include <list> 
#include <iterator>
#include "Engine.h"
#include "Tank.h"
#include "FuelTanks.h"
#include "Valve.h"
#include "Subject.h"
#include <vector>
#include "Output.h"

using namespace std;

class Simulation : public Subject {
private:
    vector<Observer*> observers;
    Engine currentEngine;
    FuelTanks currentFuelTanks;
    vector<string> functions;
    vector<string> arguments;
    bool sim_status = true;
    Output* Out;
    vector<string> out_data;
public:
    Simulation(string);
    void registerObserver(Observer* observer) override;
    void removeObserver(Observer* observer) override;
    void notifyObservers() override;
    void setState(bool status);
    bool get_commands(vector<string> file_output);
    void execute(bool, string);
    void stop_simulation();
};
