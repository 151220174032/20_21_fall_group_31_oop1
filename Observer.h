/*!
 *  @file   Observer.h
 *  @brief  Creating an interface for observers
 *  @author Koray Ayd�n - 152120171112
 *  @date   2021-01-12
 */
#pragma once
#include <iostream>

using namespace std;

class Observer {
public:
    virtual string update(bool status) = 0;
};