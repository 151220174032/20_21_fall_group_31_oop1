/*
 * FuelTanks.h
 *
 *  Created on: 6.12.2020
 *	Updated on: 20.12.2020
 *      Author: batuhanhesapdar
 */
#ifndef FUELTANKS_H_
#define FUELTANKS_H_
#include "Tank.h"
#include <vector> 
#include <iostream>
#include <string>
#include "Observer.h"
using namespace std;

class FuelTanks : public Observer {
public:
	FuelTanks();
	void add_fuel_tank(double);
	vector <string> list_fuel_tanks();
	void remove_fuel_tank(int);
	string print_fuel_tank_count();
	vector<Tank> tanks;
	virtual string update(bool status) override;
};
#endif