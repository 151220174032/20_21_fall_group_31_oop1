/*!
 *  @file   Subject.h
 *  @brief  Creating an interface for subjects
 *  @author Koray Ayd�n - 152120171112
 *  @date   2021-01-13
 */
#pragma once
#include <iostream>
#include "Observer.h"

using namespace std;

class Subject
{
public:
	virtual void registerObserver(Observer* observer) = 0;
    virtual void removeObserver(Observer* observer) = 0;
    virtual void notifyObservers() = 0;
};

