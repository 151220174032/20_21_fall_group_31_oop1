/*!
 *  @file   Valve.cpp
 *  @brief	Including valve operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-12-25
 */
#include "Valve.h"

 /*!
  * A constructor for Valve class
  */
Valve::Valve()
{
	valveStatus = false;
}
/*!
 * Open the connected tank's valve
 */
void Valve::open_valve()
{
	valveStatus = true;
}
/*!
 * Close the connected tank's valve
 */
void Valve::close_valve()
{
	valveStatus = false;
}
/*!
 * Get the valve status
 * @return returns the valve status
 */
bool Valve::getvalveStatus()
{
	return valveStatus;
}