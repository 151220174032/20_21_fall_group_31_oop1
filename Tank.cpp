/*!
 *  @file   Tank.cpp
 *  @brief	Including tank operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-12-25
 */
#include "Tank.h"

 /*!
   * A constructor for Tank class
   * @param _capacity initializing tanks capacity and connect
   */
Tank::Tank(double _capacity):capacity(_capacity)
{
	fuel_quentity = 0;
	isConnected = false;
}
/*!
 * Break the tank
 *
 */
void Tank::break_fuel_tank()
{
	broken = true;
}
/*!
 * Repair the tank
 *
 */
void Tank::repair_fuel_tank()
{
	broken = false;
}
/*!
 * Fill the tank
 * @param quantity amount to be filled
 */
void Tank::fill_tank(double quantity)
{
	fuel_quentity += quantity;
	if (fuel_quentity > capacity) fuel_quentity = capacity;
}
/*!
 * 
 * Print the tank information
 * @param id tank id
 */
string Tank::print_tank_info(int id) const
{
	string c = "\nTank " + to_string(id) + " informations are:" + "\ntank capacity: " + to_string(capacity) + " " + "tank fuel quantity: " + to_string(fuel_quentity) + " " + "is broken: ";
	if (broken == true) {
		c = c + "true";
	}
	else {
		c = c + "false";
	}
	return c;
}
/*!
 * Set the fuel quantity 
 * @param a amount to be setted
 */
void Tank::setFuel_quentity(double a)
{
	fuel_quentity = a;
}
/*!
 * Set the tank capacity 
 * @param a amount to be setted
 */
void Tank::setCapacity(double a)
{
	capacity = a;
}
/*!
 * Get the fuel quantity
 * @return returns fuel quantity
 */
double Tank::getFuel_quentity()
{
	return fuel_quentity;
}
/*!
 * Checks the connection between tank and engine
 * @return returns conncetion status 
 */
bool Tank::isItConnected()
{
	return isConnected;
}
/*!
 * Set the tank conncetion 
 * @param status tank conncetion status
 */
void Tank::setConnect(bool status)
{
	isConnected = status;
}
/*!
 * Get the tank capacity
 * @return returns tank capacity
 */
double Tank::get_capacity()
{
	return capacity;
}
/*!
 * Checks the status of the tank
 * @return returns broken tank status
 */
bool Tank::get_broken_status()
{
	return broken;
}

