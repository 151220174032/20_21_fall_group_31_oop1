/*!
 *  @file   Tank.h
 *  @brief  Including tank operations
 *  @author Koray AYDIN - 152120171112
 *  @date   2020-12-25
 */
#pragma once
#include "Valve.h"
#include <Windows.h>
#include <iostream>

using namespace std;

class Tank
{
public:
	Tank(double _capacity = 0);
	void break_fuel_tank();
	void repair_fuel_tank();
	void fill_tank(double);
	string print_tank_info(int) const;
	void setFuel_quentity(double);
	void setCapacity(double);
	double getFuel_quentity();
	bool isItConnected();
	void setConnect(bool);
	double get_capacity();
	bool get_broken_status();
	Valve connectedValve;
protected:
	double capacity;
	double fuel_quentity;
	bool isConnected;
	bool broken = false;
};

