/*!
 *  @file   Engine.cpp
 *  @brief  Operations for engine class.
 *  @author Berkay Onk - 152120181110
 *  @date   2020-12-23
 */
#include"Engine.h"

/*!
 * A constructor for Engine class
 */
Engine::Engine()
{
    this->status = false;
}
/*!
 * Starts engine.
 */
void Engine::start_engine() {
    if (internalTank.getFuel_quentity() >= 20) {
        this->status = true;
    }
}
/*!
 * Stops the engine.
 */
void Engine::stop_engine() {
    status = false;
}
/*!
 * Gives back fuel to the tank
 *
 * @param quantity quantity of fuel to give back
 */
void Engine::give_back_fuel(double quantity) {
    
    double checkFuel = internalTank.getFuel_quentity() - quantity;
    if (checkFuel < 0) {
        checkFuel = 0;
        quantity = internalTank.getFuel_quentity();
    }
    
    internalTank.setFuel_quentity(checkFuel);
    
    for (int i=0; i<connectedTankslist.size(); i++)
    {
        double empty_space = connectedTankslist.at(i).get_capacity() - connectedTankslist.at(i).getFuel_quentity();
        if (empty_space!=0 && quantity!=0 && connectedTankslist.at(i).connectedValve.getvalveStatus())
        {
            if (empty_space >= quantity) {
                connectedTankslist.at(i).fill_tank(quantity);
                break;
            }
            else {
                quantity -= empty_space;
                connectedTankslist.at(i).fill_tank(empty_space);
            }
        }
    }
}
/*!
 * Updated status of engine.
 *
 * @return returns to status
 */
bool Engine::getStatus()
{
    return status;
}
/*!
 * Engine consumes fuel.
 */
void Engine::consume()
{   
    if (internalTank.getFuel_quentity() < 20) {
        for (int i = 0; i < connectedTankslist.size(); i++) {
            double empty = 55 - internalTank.getFuel_quentity();
           
            if (connectedTankslist.at(i).getFuel_quentity() >= empty && connectedTankslist.at(i).connectedValve.getvalveStatus()) {
                connectedTankslist.at(i).setFuel_quentity(connectedTankslist.at(i).getFuel_quentity() - empty);
                internalTank.setFuel_quentity(55);
                break;
            }
            else if (connectedTankslist.at(i).getFuel_quentity() < empty && connectedTankslist.at(i).connectedValve.getvalveStatus()) {
                internalTank.setFuel_quentity(internalTank.getFuel_quentity() + connectedTankslist.at(i).getFuel_quentity());
                connectedTankslist.at(i).setFuel_quentity(0);
            }
        }
    }
    if (internalTank.getFuel_quentity() > 20) {
        internalTank.setFuel_quentity(internalTank.getFuel_quentity() - fuel_per_second);
    }
    else status = false;
}
/*!
 * waits for engine to consume fuel
 *
 * @param time time to wait for consumption
 */
int Engine::wait(int time)
{
    for (int i = 0; i < time; i++) {
        if (status == false) return i-1;
        consume();
    }
    return time;
}
/*!
 * connects fuel tank to engine
 *
 * @param id id of the tank
 */
void Engine::connect_fuel_tank_to_engine(Tank id)
{
    connectedTankslist.push_back(id);
    if (internalTank.getFuel_quentity() < 20) {
        for (int i = 0; i < connectedTankslist.size(); i++) {
            if (connectedTankslist.at(i).getFuel_quentity() > 55) {
                connectedTankslist.at(i).setFuel_quentity(connectedTankslist.at(i).getFuel_quentity() - 55);
                internalTank.setFuel_quentity(55);
                break;
            }
        }
    }
}
/*!
 * disconnects fuel tank from engine
 *
 * @param id id of the tank
 */
void Engine::disconnect_fuel_tank_from_engine(int id)
{
    connectedTankslist.at(id).setCapacity(0);
}
/*!
 * lists connected tanks
 *
 * @return return to the out
 */
vector<string> Engine::list_connected_tanks()
{
    vector<string> out;
    out.push_back("\nHere are the connected fuel tanks informations: ");
    for (int i = 0; i < connectedTankslist.size(); i++)
    {
        if (connectedTankslist.at(i).get_capacity() != 0) {
            out.push_back("\nTank " + to_string(i + 1) + " information: ");
            out.push_back(" capacity is: " + to_string(connectedTankslist.at(i).get_capacity()));
            out.push_back(" fuel_quantity is: " + to_string(connectedTankslist.at(i).getFuel_quentity()));
            if (connectedTankslist.at(i).get_broken_status())
                out.push_back(" Is broken: true");
            else
                out.push_back(" Is broken: false");

            if (connectedTankslist.at(i).connectedValve.getvalveStatus()) {
                out.push_back(" Is valve open: true");
            }
            else {
                out.push_back(" Is valve open: false");
            }
        }
    }
    out.push_back("\n");
    return out;
}
/*!
 * it checks of the status and informs
 *
 * @param status status of the subject 
 * @return returns status of the engine observer
 */
string Engine::update(bool status)
{
    if (!status) {
        return "\nEngine: Simulation stopped.\n";
    }
}